package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static void makeScreenshot(WebDriver driver, String testName) {
        DateFormat dateFormat = new SimpleDateFormat("HHmmss_yyyyMMdd");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        String name = "screenshot_" + testName + "_" + dateFormat.format(date) + ".png";
        String resultsPath = System.getProperty("user.dir") + "/test-reports/screenshots" + "/" + name;

        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(resultsPath);
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
