package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class MainPage {
    private static final Logger LOG = LoggerFactory.getLogger(MainPage.class);
    private WebDriver driver;
    private Wait<WebDriver> wait;

    @FindBy(xpath = "//li[@class='gpn-menu-list-item cameras']")
    private WebElement cameras;

    @FindBy(xpath = "//span[@class='icon-exit-stroke']")
    private WebElement miniCartPopUpClose;

    @FindBy(xpath = "//div[@class='gpn-flash-message gpn-gdpr-text-message']")
    private WebElement gdprInfo;

    @FindBy(xpath = "//span[@class='gpn-gdpr-close-text']")
    private WebElement gdprAcceptButton;

    @FindBy(xpath = "//a[@class='mini-cart-content']")
    private WebElement miniCart;

    @FindBy(xpath = "//a[@class='mini-cart-viewcart-cta btn-round']")
    private WebElement viewCart;

    @FindBy(xpath = "//div[@class='ReactModal__Content ReactModal__Content--after-open gpn-email-capture-lightbox-modal']")
    private WebElement newsLetterModal;

    @FindBy(xpath = "//button[@class='modal-close']")
    private WebElement buttonCloseNewLetterModal;


    public MainPage(WebDriver driver) {
        this.driver = driver;
        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(15))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);
        PageFactory.initElements(this.driver, this);
    }

    public void goToCameras() {
        wait.until(ExpectedConditions.visibilityOf(cameras)).click();
    }

    public void addToCart(String cameraName) {
        String xpath = "//div[@class='" + cameraName +
                "-section section-image']//button[@class='add-to-cart btn btn-medium btn-round']";
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
        } catch (ElementClickInterceptedException e) {
            try {
                if (newsLetterModal.isDisplayed())
                    buttonCloseNewLetterModal.click();
            } catch (NoSuchElementException ex) {
                LOG.info("No element present");
            }
            this.confirmGdpr();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
        }
    }

    public void selectLanguage(String country, String language) {
        String xpath = "//button[country-language country-language-" + language + "-" + country + "']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
    }

    public void closeMiniCartPopUpClose() {
        wait.until(ExpectedConditions.visibilityOf(miniCartPopUpClose)).click();
    }

    public void confirmGdpr() {
        try {
            wait.until(ExpectedConditions.visibilityOf(gdprInfo));
            wait.until(ExpectedConditions.visibilityOf(gdprAcceptButton)).click();
        } catch (Exception e) {
            LOG.info("No gdpr to be confirm.");
        }
    }

    public void viewCart() {
        wait.until(ExpectedConditions.elementToBeClickable(viewCart)).click();
    }
}
