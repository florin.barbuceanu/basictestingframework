package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.List;

public class ShoppingCartPage {
    private WebDriver driver;
    private Wait<WebDriver> wait;

    @FindBy(xpath = "//div[@class='name']")
    private List<WebElement> allProductNamesElements;

    @FindBy(xpath = "//div[@class='cart-items']")
    private WebElement cartItems;

    @FindBy(xpath = "//div[@class='cart-empty']")
    private WebElement cartEmpty;

    public ShoppingCartPage(WebDriver driver) {
        this.driver = driver;
        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(15))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);
        PageFactory.initElements(this.driver, this);
    }

    public void waitPageToBeLoaded() {
        wait.until(ExpectedConditions.visibilityOf(cartItems));
    }

    public boolean validateProductExistOnCart(String productName) {
        for (WebElement element : allProductNamesElements)
            if (element.getText().equals(productName))
                return true;
        return false;
    }

    public void deleteElementFromCart(String productName) {
        String xpath = "//button[@data-name='" + productName + "']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
    }

    public boolean cartIsEmpty() {
        try {
            return wait.until(ExpectedConditions.visibilityOf(cartEmpty)).isDisplayed();
        } catch (Exception ex) {
            return false;
        }
    }
}
