package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.MainPage;
import pageObjects.ShoppingCartPage;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ShoppingCartTest extends BaseTest {

    private MainPage mainPage;
    private ShoppingCartPage shoppingCartPage;

    @DataProvider(name = "camerasName")
    public Iterator<Object[]> camerasToRemove() {
        Collection<Object[]> dp = new ArrayList<>();
        // cameras Name
        dp.add(new String[]{"HERO8 Black", "MAX"});
        dp.add(new String[]{"MAX", ""});

        return dp.iterator();
    }

    public void setUp(String language, String country) {
        driver.get("http://" + hostname + "/" + language + "/" + country);
        mainPage = new MainPage(driver);
    }

    @Parameters({"language", "country"})
    @Test
    public void addItemsToBag(String language, String country, Method method) {
        startExtentReports(method.getName() + "_" + country + "_" + language, "Add 2 cameras to Shopping Cart");
        logTestInfo("Run Test for language: " + language + " and country: " + country);
        setUp(language, country);

        logTestInfo("Go to camera categories");
        mainPage.goToCameras();

        logTestInfo("Add hero8 camera to shopping bag");
        mainPage.addToCart("hero8");

        logTestInfo("Close mini cart pop up");
        mainPage.closeMiniCartPopUpClose();

        logTestInfo("Add max camera to shopping bag");
        mainPage.addToCart("max");

        logTestInfo("Go to see cart");
        mainPage.viewCart();

        shoppingCartPage = new ShoppingCartPage(driver);
        logTestInfo("Wait for Shopping Cart to be Loaded");
        shoppingCartPage.waitPageToBeLoaded();

        logTestInfo("Verify that product HERO8 Black is on cart");
        Assert.assertTrue(shoppingCartPage.validateProductExistOnCart("HERO8 Black"));
        logTestInfo("Verify that product MAX is on cart");
        Assert.assertTrue(shoppingCartPage.validateProductExistOnCart("MAX"));
    }

    @Test(dependsOnMethods = "addItemsToBag", dataProvider = "camerasName")
    public void deleteElementsFromCart(String removeCameraName, String remainingCameraName, Method method) {
        startExtentReports(method.getName() + "_" + removeCameraName, "Remove cameras from Shopping Cart");
        logTestInfo("Run Tests to remove cameras");

        logTestInfo("Remove product " + removeCameraName + " from cart");
        shoppingCartPage.deleteElementFromCart(removeCameraName);

        logTestInfo("Verify that product " + removeCameraName + " is deleted cart");
        Assert.assertFalse(shoppingCartPage.validateProductExistOnCart(removeCameraName));

        if (remainingCameraName.length() > 0) {
            shoppingCartPage.waitPageToBeLoaded();
            logTestInfo("Verify that product " + remainingCameraName + " remain on cart");
            Assert.assertTrue(shoppingCartPage.validateProductExistOnCart(remainingCameraName));
        } else {
            logTestInfo("Verify that cart is empty");
            Assert.assertTrue(shoppingCartPage.cartIsEmpty());
        }
    }

    @Test(dependsOnMethods = "deleteElementsFromCart")
    public void deleteElementsFromEmptyCart(Method method) {
        startExtentReports(method.getName(), "Remove cameras from empty Shopping Cart");
        logTestInfo("Try to remove a camera from empty cart");
        shoppingCartPage.deleteElementFromCart("MAX");
    }
}
