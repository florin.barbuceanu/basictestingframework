package tests;

import Listeners.ExtentReports.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import utils.Util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import static utils.WebBrowsers.getDriver;

public class BaseTest {
    private static final Logger LOG = LoggerFactory.getLogger(BaseTest.class);
    protected WebDriver driver = null;
    String hostname;

    /**
     * Method for starting a web browser driver.
     * It is intended to be run before each test.
     */

    public WebDriver getCurrentDriver() {
        return this.driver;
    }

    @BeforeTest
    public void beforeTest() {
        String browserType;
        // -D cmd line parameters
        LOG.info("CMD line browser:" + System.getProperty("browserType"));
        browserType = System.getProperty("browserType");

        // Get default value for browser if not specified
        try {
            InputStream input = readResourceFile("framework.properties");
            Properties prop = new Properties();
            prop.load(input);
            if (browserType == null) {
                LOG.info("Setup browser from config");
                browserType = prop.getProperty("browserType");
                LOG.info("PROPERTY BROWSER Value: " + prop.getProperty("browserType"));
            }
            driver = getDriver(browserType);
            hostname = prop.getProperty("hostname");
            LOG.info("hostname: " + prop.getProperty("hostname"));
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
        }
    }

    private void closeDriver() {
        if (driver != null) {
            driver.quit();
        } else {
            LOG.info("No driver to be closed.");
        }
    }


    @AfterTest(alwaysRun = true)
    public void closeBrowserAtEnd() {
        LOG.info("Close driver on AfterTest");
        closeDriver();
    }

    /**
     * This Method it is used to make screen shots any time when a test fail
     */
    @AfterMethod
    public void triggerScreenshoot(ITestResult result) {
        //using ITestResult.FAILURE is equals to result.getStatus then it enter into if condition
        if (ITestResult.FAILURE == result.getStatus() && ((RemoteWebDriver) driver).getSessionId() != null)
            Util.makeScreenshot(driver, result.getName());
    }

    @AfterClass(alwaysRun = true)
    public void closeBrowserAtEndOfRun() {
        LOG.info("Close driver on AfterClass");
        closeDriver();
    }

    void startExtentReports(String methodName, String description) {
        ExtentTestManager.startTest(methodName, description);
    }

    void logTestInfo(String message) {
        ExtentTestManager.getTest().log(LogStatus.INFO, message);
        LOG.info(message);
    }

    /**
     * This Method it is used to read resources files
     */
    private InputStream readResourceFile(String resource) throws IOException {
        return getClass().getResource("/" + resource).openStream();
    }

}
