package Listeners.ExtentReports;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {

    private static ExtentReports extent;

    public synchronized static ExtentReports getReporter() {
        if (extent == null) {
            //Set HTML reporting file location
            String workingDir = System.getProperty("user.dir");
            System.out.println("Save dir:" + workingDir + "/test-reports/ExtentReportResults.html");
            extent = new ExtentReports(workingDir + "/test-reports/ExtentReportResults.html", true);
        }
        return extent;
    }
}