# Basic Testing Framework

This project is hosted on gitlab at https://gitlab.com/florin.barbuceanu/basictestingframework

The project is a maven project so all dependencies are getting automatically.

## Development

The Framework is a Java application.  Import it into your IDE (IntelliJ recommended) as a Maven module and then your IDE should be able to build it.
Technologies used to develop framework are:
* Java 8
* Maven
* Selenium
* TestNG
* BoniGarcia
* Extended Reports
* Loopback

## Run Tests

You can run all tests created using the following command line command from project location: 

    * mvn clean test -DsuiteXmlFile=allTest.xml -DbrowserType=chrome

Make sure that you have installed the maven on your system.
You can find here more info: http://maven.apache.org/install.html

### Reports 

You can find the reports after you run the tests on the following location under project:
* **test-reports**, please open on any browser the _ExtentReportResults.html_ where can found
 beautiful, interactive and detailed reports for your tests. 
* **_target/surefire-reports/junitreports_** - can be found junitReports 
* under **target/surefire-reports** you can found multiple reports on different formats, as example _emailable-report.html_
that can be used to send results over email to entire team.  

Also for debugging more related to the test run under project on folder **logs** can be found app logs. 





